package com.springboot.restapivalidation.exception;

import java.util.Map;
import org.springframework.http.HttpStatus;

//This class represents the body of the custom exception
class ApiException {
	
	private String status;
	private int code;
	private String message;
	private String stackTrace;
	private Map<String, String> errors;
	
	public ApiException(String status, int code, String message) {
		super();
		this.status = status;
		this.code = code;
		this.message = message;
	}
	
	public ApiException(String status, int code, String message, String stackTrace) {
		this(status, code, message);
		this.stackTrace = stackTrace;
	}
	
	public ApiException(String status, int code, String message, Map<String, String> errors) {
		this(status, code, message);
		this.errors = errors;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}

	public Map<String, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}
	
	
}
