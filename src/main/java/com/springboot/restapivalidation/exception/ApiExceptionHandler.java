package com.springboot.restapivalidation.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class ApiExceptionHandler {
	
	@ExceptionHandler(value = {CustomDataNotFoundException.class})
	public ResponseEntity<ApiException> handleUserNotFoundException(CustomDataNotFoundException ex){
		return new ResponseEntity<>(new ApiException(ex.status.name(), ex.status.value(), ex.getMessage()), ex.status);
	}
	
	@ExceptionHandler(value = {MethodArgumentNotValidException.class})
	public ResponseEntity<ApiException> handleInvalidArgument(MethodArgumentNotValidException ex) {
		Map<String,String> map = new HashMap<>();
		ex.getBindingResult().getFieldErrors().forEach(error -> {
			map.put(error.getField(), error.getDefaultMessage());
		});
		return new ResponseEntity<>(new ApiException(HttpStatus.BAD_REQUEST.name(), 
				HttpStatus.BAD_REQUEST.value(), "Data is not valid", map), HttpStatus.BAD_REQUEST);
	}
	
	//Method to cover remaining cases
	@ExceptionHandler(Exception.class)
	public  ResponseEntity<ApiException> handleException(Exception ex){
		StringWriter stWriter = new StringWriter();
		PrintWriter ptWriter = new PrintWriter(stWriter);
		ex.printStackTrace(ptWriter);
		return new ResponseEntity<>(new ApiException(HttpStatus.INTERNAL_SERVER_ERROR.name(), 
				HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), stWriter.toString()), 
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
