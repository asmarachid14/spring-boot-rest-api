package com.springboot.restapivalidation.exception;

import org.springframework.http.HttpStatus;

public class CustomDataNotFoundException  extends RuntimeException{

	private static final long serialVersionUID = 1L;

	HttpStatus status;
	public CustomDataNotFoundException(String message, HttpStatus status) {
		super(message);
		this.status = status;
	}

}
