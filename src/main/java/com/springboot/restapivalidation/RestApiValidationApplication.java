package com.springboot.restapivalidation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.springboot.restapivalidation.model.User;
import com.springboot.restapivalidation.repository.UserRepository;

@SpringBootApplication
public class RestApiValidationApplication {

	private static final Logger log = LoggerFactory.getLogger(RestApiValidationApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(RestApiValidationApplication.class, args);
	}
	
	@Bean
	CommandLineRunner initDataBase(UserRepository repository) {
		return args ->{
			User uss = new User(0, "anas",
					"anas@gmail", "0761574451", "m", "Djiboutian", 20, null);
			uss.setSusp(SuspendType.MANUAL);
			log.info("Preloading: {}", repository.save(uss));
			log.info("Preloading: {}", repository.save(new User(0,
					"Oumalkaire", "oumal@gmail", "0761574451", "f",
					"Djiboutian", 20, SuspendType.AUTOMATIC)));
			log.info("Preloading: {}", repository.save(new User(0,
					"Oumalkaire", "oumal@gmail", "0761574451", "f",
					"Djiboutian", 20, SuspendType.MANUAL)));
		};
	}
	
}
