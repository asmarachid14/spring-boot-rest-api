package com.springboot.restapivalidation.controller;

import java.util.List;

import com.springboot.restapivalidation.model.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.restapivalidation.dto.UserDto;
import com.springboot.restapivalidation.mapper.MapObject;
import com.springboot.restapivalidation.service.UserService;

import jakarta.validation.Valid;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;
	@Autowired
	MapObject mapper;
	
	@PostMapping
	public ResponseEntity<HttpStatus> saveUser(@RequestBody @Valid UserDto userDto){
		User target = new User();
		BeanUtils.copyProperties(userDto, target);
		System.out.println("source: "+userDto);
		System.out.println("target: "+target);
		System.out.println("Commit added by the main branch");
		userService.saveUser(target);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping
	public ResponseEntity<List<UserDto>> getAllUsers() {
		return new ResponseEntity<>(mapper.mapUsersToUserAllDtos(userService.getAllUsers()), HttpStatus.OK);
	}
	
	@GetMapping("{id}")
	public ResponseEntity<UserDto> getUser(@PathVariable int id) {
		//return new ResponseEntity<>(mapper.mapUserToUserDto(userService
		// .getUser(id)), HttpStatus.OK);
		throw new
		ResponseStatusException(HttpStatus.BAD_REQUEST, "error in the request");
	}
	
	@GetMapping("/startingWith")
	public ResponseEntity<List<UserDto>> getUsersByNameStartingWithKeyword(@RequestParam String prefix){
		return new ResponseEntity<List<UserDto>>(mapper.mapUsersToUserAllDtos
				(userService.getUsersByNameStartingWithKeyword(prefix)), HttpStatus.OK);
	}
	
	@GetMapping("/endingWith")
	public ResponseEntity<List<UserDto>> getUsersByNameEndingWithKeyword(@RequestParam String suffix){
		return new ResponseEntity<List<UserDto>>(mapper.mapUsersToUserAllDtos
				(userService.getUsersByNameEndingWithKeyword(suffix)), HttpStatus.OK);
	}

	@GetMapping("/update/{id}")
	public ResponseEntity<User> updateUser(@PathVariable int id) {
		return new ResponseEntity<>(userService.updateUser(id), HttpStatus.OK);
	}
}
