package com.springboot.restapivalidation.service;

import java.util.ArrayList;
import java.util.List;

import com.springboot.restapivalidation.dto.UserDto;
import jakarta.persistence.criteria.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.springboot.restapivalidation.exception.CustomDataNotFoundException;
import com.springboot.restapivalidation.model.User;
import com.springboot.restapivalidation.repository.UserRepository;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

	@Autowired
	UserRepository repository;
	
	public User saveUser(User user) {
		return repository.save(user);
	}
	
	public List<User> getAllUsers(){
		return repository.findAll();
	}
	
	public User getUser(int id) {
		return repository.findById(id).orElseThrow(()->new CustomDataNotFoundException("User not found", HttpStatus.NOT_FOUND));
	}
	
	public List<User> getUsersByNameStartingWithKeyword(String prefix){
		return repository.findByNameStartingWith(prefix);
	}
	
	public List<User> getUsersByNameEndingWithKeyword(String prefix){
		return repository.findByNameEndingWith(prefix);
	}
	
	public List<User> getUsersByAgeBetween(int startAge, int endAge){
		return repository.findByAgeBetween(startAge, endAge);
	}

	@Transactional
	public User updateUser(int id){
		User uss = repository.findById(id).get();
		uss.setNationality("Deutch");
		return repository.save(uss);
	}

}
