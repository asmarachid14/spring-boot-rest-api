package com.springboot.restapivalidation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.restapivalidation.model.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRepository extends JpaRepository<User, Integer>,
		JpaSpecificationExecutor<User> {
	
	List<User> findByNameStartingWith(String prefix);
	
	List<User> findByNameEndingWith(String suffix);
	
	List<User> findByNameContaining(String infix);
	
	List<User> findByNameLike(String likePattern);
	
	List<User> findByAgeGreaterThan(int age);
	
	List<User> findByAgeBetween(int startAge, int endAge);
	
	List<User> findByAgeIn(List<Integer> ages);
	
	List<User> findByIdOrGender(int id, String gender);
	
	List<User> findByNameOrderByName(String name);
		
	List<User> findTop3ByAge(int age);
}
