package com.springboot.restapivalidation.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.springboot.restapivalidation.dto.UserDto;
import com.springboot.restapivalidation.model.User;

@Component
public class MapObject {
	
	public UserDto mapUserToUserDto(User user) {
		if(user == null) return null;
		
		UserDto userDto = new UserDto();
		userDto.setId(user.getId());
		userDto.setName(user.getName());
		userDto.setEmail(user.getEmail());
		userDto.setMobile(user.getMobile());
		userDto.setGender(user.getGender());
		userDto.setNationality(user.getNationality());
		userDto.setAge(user.getAge());
		return userDto;	
	}
	
	public User mapUserDtoToUser(UserDto userDto) {
		if(userDto == null) return null;
		
		User user = new User();
		user.setName(userDto.getName());
		user.setEmail(userDto.getEmail());
		user.setMobile(userDto.getMobile());
		user.setGender(userDto.getGender());
		user.setNationality(userDto.getNationality());
		user.setAge(userDto.getAge());
		return user;	
	}
	
	public List<UserDto> mapUsersToUserAllDtos(List<User> listUser){
		
		if(listUser == null) return null;
		
		List<UserDto> listUserDto = new ArrayList<UserDto>(listUser.size());
		for(User user: listUser) {
			listUserDto.add(mapUserToUserDto(user));
		}
		return listUserDto;
	}
}
