package com.springboot.restapivalidation.model;

import jakarta.persistence.*;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "userr")
public class User {
	
	@Id
	@GeneratedValue
	private int id;
	private String name;
	private String email;
	private String mobile;
	private String gender;
	private String nationality;
	private int age;
	
/*	public User() {}

	public User(int id, String name, String email, String mobile,
				String gender, String nationality, int age) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.mobile = mobile;
		this.gender = gender;
		this.nationality = nationality;
		this.age = age;
	}

	public User(int id, String name, String email, String mobile,
				String gender, String nationality, int age, SuspendType suspendType) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.mobile = mobile;
		this.gender = gender;
		this.nationality = nationality;
		this.age = age;
		this.suspendType = suspendType;
	}*/

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", mobile='" + mobile + '\'' +
				", gender='" + gender + '\'' +
				", nationality='" + nationality + '\'' +
				", age=" + age +
				'}';
	}
}
